import Express from 'express'
import ArticleRoutes from './routes/productRoutes.js'
import dotenv from 'dotenv'
import MongoDb from './database/mongoDB.js'
import { errorHandler,notFound} from './middleware/ErrorMiddleware.js'

dotenv.config()

MongoDb()

const app = Express()

app.get('/',(req,res)=>{
    res.send('Api is running')
})

app.use('/api/articleroutes',ArticleRoutes)

app.use(notFound)
app.use(errorHandler)

const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
	console.log(`Server running on ${process.env.NODE_ENV} at port ${PORT}`.yellow.bold);
});
