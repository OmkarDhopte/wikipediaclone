import Express from 'express'
import asyncHandler from 'express-async-handler'
import Article1 from '../data/AncientEgyptianLiterature.js'
import Article2 from '../data/EgyptianLanguage.js'

const router = Express.Router()

router.get('/ancientegyptliterature', asyncHandler(async (req,res)=>{
    const article1 = await Article1
    res.json(article1)
}))

router.get('/egyptianlanguage', asyncHandler(async (req,res)=>{
    const article2 = await Article2
    res.json(article2)
}))

export default router
