import mongoose from 'mongoose'
import colors from 'colors'

const database = async () =>{
    try {
        const conn = await mongoose.connect(process.env.URI)
        console.log(`dataBase is connected at: ${conn.connection.host}`.cyan.underline)
    } catch (err) {
        console.log(`Error at ${err.message}`.red.underline)
        process.exit(1)
    }
}

export default database