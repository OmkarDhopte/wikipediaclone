import { BrowserRouter,Route,Routes } from 'react-router-dom'
import Header from './components/Headers'
import FrontPage from './Screens/FrontPage'
import Ancientegyptliterature from './Screens/ancientegyptliterature'

function App() {
  return (
    <BrowserRouter>
    <Header/>
    <Routes >
      <Route path='/' element={<FrontPage/>}/>
      <Route path='/ancientegyptliterature' element={<Ancientegyptliterature/>}/>
    </Routes>
    </BrowserRouter>
  );
}

export default App;
