import { Box,
    Flex, 
    Grid, 
    Image, 
    Text,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Button,
    Link,
    Popover,
    PopoverTrigger,
    PopoverArrow,
    PopoverCloseButton,
    PopoverContent,
    PopoverBody,
    PopoverHeader,
    UnorderedList,
    List
} from '@chakra-ui/react'
import { AiOutlineAlignLeft } from "react-icons/ai"
import { BsList,BsThreeDots } from "react-icons/bs"
import WikiLogo from '../Images/Wikipedia-logo-header.png'
import { CiSearch } from "react-icons/ci";
import { Link as RouterLink } from 'react-router-dom'
import { useState } from 'react'

const Header = function () {

    const [sideBar, setSideBar] = useState(true)

    const fontFamily = 'Arial'
    const LetterSpacing='0.1em'
    const sidebarTextColor = 'blue'
    return(
        <>
        <Flex flexDirection='row' width='100%' padding='0.5% 3%' justifyContent='space-between' alignItems='center'>
            <Box>
                <Flex dir='row' alignItems='center'>
                    <Box display={{base:'none',md:'block'}}>
                        {sideBar ? 
                        (<Popover>
                            <PopoverTrigger>
                                <Button padding='0px' backgroundColor='rgba(127, 17, 224, 0)'><BsList/></Button>
                            </PopoverTrigger>
                            <PopoverContent>
                                <PopoverArrow/>
                                <PopoverHeader fontSize='16px'>
                                    <Text as='strong' marginLeft='2rem'>
                                        Main Menu
                                    </Text>
                                    <Button marginLeft='3rem' fontSize='12px' _hover={{backgroundColor:'rgba(127, 17, 224, 0)'}} onClick={()=>setSideBar(!sideBar)}>
                                        Move to Sidebar
                                    </Button>
                                </PopoverHeader>
                                <PopoverBody marginLeft='10px'>
                                    <UnorderedList spacing='10px'>
                                        <List>
                                            <Link as={RouterLink} to='/' textColor={sidebarTextColor}>Main Page</Link><br/>
                                        </List>
                                        <List>
                                            <Link as={RouterLink} to='/content' textColor={sidebarTextColor} >Contents</Link><br/>
                                        </List>
                                        <List>
                                            <Link as={RouterLink} to='/currentEvents' textColor={sidebarTextColor}>Current events</Link><br/>
                                        </List>
                                        <List>
                                            <Link as={RouterLink} to='/about' textColor={sidebarTextColor}>About Wikipedia</Link><br/>
                                        </List>
                                        <List>
                                            <Link as={RouterLink} to='/contact' textColor={sidebarTextColor}>Contact Us</Link><br/>
                                        </List>
                                        <List>
                                            <Link as={RouterLink} to='/donate' textColor={sidebarTextColor}>Donate</Link><br/>
                                        </List>
                                    </UnorderedList>
                                </PopoverBody>
                            </PopoverContent>
                        </Popover>)
                        :
                        <Box>
                            <Flex direction='column'>
                            </Flex>
                        </Box>}
                    </Box>
                    <Box marginLeft='2rem'>
                        <Link as={RouterLink} to='/' _hover={{textDecor:'none'}}>
                            <Grid gridTemplateColumns='0.5fr 2fr' alignItems='center'>
                                <Box display={{base:'block',md:'none'}}>
                                    <AiOutlineAlignLeft/>
                                </Box>
                                <Box display={{base:'none',md:'block'}}>
                                    <Image src={WikiLogo} height='2.4rem' w='5rem'/>
                                </Box>
                                <Box marginLeft='1rem'>
                                    <Flex flexDir='column'>
                                        <Text fontFamily={fontFamily} letterSpacing={LetterSpacing} fontSize='1rem'><Box as='abbr' fontSize='1.3rem'>W</Box>IKIPEDI<Box as='abbr' fontSize='1.3rem'>A</Box></Text>
                                        <Text fontFamily={fontFamily} fontSize='0.8rem'> The Free Enclyopedia </Text>
                                    </Flex>
                                </Box>
                            </Grid>
                        </Link>
                    </Box>
                    <Box marginLeft='3rem'>
                        <InputGroup>
                            <InputLeftElement height='2rem'>
                                <CiSearch />
                            </InputLeftElement>
                            <Input border='solid black 1px' type='text' placeholder='Search Wikipedia' width='30rem' height='2rem' borderRadius='0px'/>
                            <InputRightElement borderRadius='0px' width='60px' height='2rem'>
                                <Button border='solid black 1px' borderRadius='0px' height='2rem'><Text as='b'>Search</Text></Button>
                            </InputRightElement>
                        </InputGroup>
                    </Box>
                </Flex>
            </Box>
            <Box>
                <Flex dir='column' marginTop='1.2rem' columnGap='20px'>
                    <Text fontSize='medium' textColor='blue'>
                        <Link as={RouterLink} to='/createAccount'>Create Account</Link>
                    </Text>
                    <Text fontSize='medium' textColor='blue'>
                        <Link as={RouterLink} to='/login'>Log in</Link>
                    </Text>
                    <Box>
                        <Popover>
                            <PopoverTrigger>
                                <Button border='none' bgColor='rgba(127, 17, 224, 0)'><BsThreeDots/></Button>
                            </PopoverTrigger>
                            <PopoverContent>
                                <PopoverArrow/>
                                <PopoverCloseButton/>
                                <PopoverBody padding='20px' >
                                    Pages for logged out edditor <br/>
                                    (Learn more)<br/>
                                    <Link as={RouterLink} to='/contributions' >Contributions</Link><br/>
                                    <Link as={RouterLink} to='/talk'>Talk</Link>
                                </PopoverBody>
                            </PopoverContent>
                        </Popover>
                    </Box>
                </Flex>
            </Box>
        </Flex>
        </>
    )
}

export default Header