import Thunk from 'redux-thunk'
import { applyMiddleware,combineReducers,createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

const reducer = combineReducers([])

const initialState = {}

const middlewares = [Thunk]

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
)

export default store
