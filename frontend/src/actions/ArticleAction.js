import {
    ARTICLE_REQUEST,
    ARTICLE_SUCESS,
    ARTICLE_FAIL
} from '../constants/ArticleConstants'
import axios from 'axios'

export const Article1 = () => async (dispatch) => {
    try {
        dispatch({type:ARTICLE_REQUEST})

        const Article1 = await axios.get('/api/articleroutes/ancientegyptliterature')
        const Article2 = await axios.get('/api/articleroutes/egyptianlanguage')

        let data = [Article1,Article2]

        dispatch({ type: ARTICLE_SUCESS, payload:data})
    } catch (error) {
        dispatch({ type: ARTICLE_FAIL, payload:error.response && error.response.data.message ? error.response.data.message : error.message })
    }
}
