import {
    Tabs,
    TabList,
    TabPanel,
    Tab,
    TabPanels,
    Box,
    Text,
    Grid,
    Divider,
    Link,
    Card,
    CardHeader,
    CardBody,
    Heading,
    Image,
    List,
    UnorderedList,
    ListItem,
    GridItem,
    Icon,
    Flex,
    AbsoluteCenter,
    Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    Portal,
    PopoverArrow,
    PopoverHeader,
    PopoverBody,
    Stack,
    Code,
    PopoverCloseButton,
    Input,
    InputGroup,
    InputLeftAddon,
    Accordion,
    AccordionButton,
    AccordionItem,
    AccordionIcon,
    AccordionPanel,
    Select,
    Radio,
  } from '@chakra-ui/react'
  import {Link as RouterLink} from 'react-router-dom'
  import { useState } from 'react'
  import Egypttoday from '../Images/Hieroglyphs_from_the_tomb_of_Seti_I.jpg'
  import DidyouKnow from '../Images/659px-Caragiale27.jpg'
  import MarsFly from '../Images/Mars_helicopter_on_sol_46.png'
  import onThisDay from '../Images/448px-Reliquary_of_Pope_Sylvester_I_PEAE_Zadar.jpg'
  import TodayPicture from '../Images/Domergues_leaf_chameleon_(Brookesia_thieli)_Andasibe.jpg'
  import { SiWikimediacommons } from "react-icons/si"
  import MediaWiki from '../Images/MediaWiki.png'
  import metaWiki from '../Images/MetaWiki.png'
  import WikiBook from '../Images/WikiBooks.png'
  import WikiData from '../Images/WikiData.png'
  import WikiNews from '../Images/WikiNews.png'
  import WikiQuote from '../Images/WikiQuote.png'
  import WikiSource from '../Images/WikiSource.png'
  import WikiSpecies from '../Images/WikiSpecies.png'
  import Wikiversity from '../Images/Wikiversity.png'
  import WikiVoyage from '../Images/WikiVoyage.png'
  import Wiktionary from '../Images/Wiktionary.png'
  import { FaAngleDown } from "react-icons/fa"
  import { BiSearch, BiSolidLock } from "react-icons/bi"
  import sourceCode from '../components/Wikicodes'
  import { DiAptana } from "react-icons/di";
  import { ExternalLinkIcon } from '@chakra-ui/icons'
  
  const FrontPage = () => {
  
    const [test,setTest] = useState(0)
    const [tab,setTab] = useState(0)
    const [tab1, setTab1] = useState(0)
    return (
      <>
        <Grid gridTemplateColumns='1fr 6fr' fontFamily='Arial'>
          <Box>
            {test}
          </Box>
          <Box as='section' padding='1rem 4rem' fontFamily='Georgia Ref Regular' marginRight='8rem'>
            <Tabs onChange={(e=0)=>setTest(e)}>
              <TabPanels>
                {
                  tab === 0 && !test ? <TabPanel></TabPanel>: tab === 1 && !test ? <TabPanel fontSize='25px'>View Source For Main Page <Divider border='solid grey 0.5px'/></TabPanel>
                   : <TabPanel fontSize='25px'>Main Page: Revision history <Divider border='solid grey 0.5px'/></TabPanel>
                }{
                  tab1 === 0 && test ? <TabPanel fontSize='25px'>Talk:Main Page<Divider border='solid grey 0.5px'/></TabPanel>: tab1 === 1 && test ? <TabPanel fontSize='25px'>View source for Talk:Main Page <Divider border='solid grey 0.5px'/></TabPanel> : <TabPanel fontSize='25px'>Talk:Main Page: Revision history<Divider border='solid grey 0.5px'/></TabPanel>
                }
              </TabPanels>
              <TabList>
                <Tab>Main Page</Tab>
                <Tab>Talk</Tab>
              </TabList>
              {
                !test ?
                <Tabs align='end' onChange={(i)=> setTab(i)} isLazy>
                  <TabList>
                    <Tab>Read</Tab>
                    <Tab>View Source</Tab>
                    <Tab>View History</Tab>
                    <Box>
                      <Popover>
                        <PopoverTrigger>
                          <Button bg='none' fontWeight='none' _hover={{bg:'#F7FAFC'}}>
                            Tools
                            <FaAngleDown/>
                          </Button>
                        </PopoverTrigger>
                        <Portal>
                          <PopoverContent>
                            <PopoverArrow />
                            <PopoverHeader>
                              Tools
                            </PopoverHeader>
                            <PopoverHeader>
                              General
                            </PopoverHeader>
                            <PopoverBody>
                              <Stack direction='column'>
                                <Text textColor='blue'>What links here</Text>
                                <Text>Related changes</Text>
                                <Text>Special pages</Text>
                                <Text>Permanant link</Text>
                                <Text>Page information</Text>
                              </Stack>
                            </PopoverBody>
                          </PopoverContent>
                        </Portal>
                      </Popover>
                    </Box>
                  </TabList>
                  <TabPanels>
                    <TabPanel>
                      <Box 
                      height='8rem' 
                      width='100%' 
                      backgroundColor='rgba(237, 242, 247, 1)' 
                      border='solid #CBD5E0 1px'
                      textAlign='center'
                      padding='1.2rem'
                      fontSize='24px'
                      >
                        Welcome to <Link as={RouterLink} to='/' color='blue'>Wikipedia</Link>,<br/>
                        <Text fontSize='15px' fontFamily='Calibri (Body)'> the free encylopedia that anyone can edit. </Text>
                        <Text fontSize='15px' fontFamily='Calibri (Body)'> 6,776,554 articles in English </Text>
                      </Box>
                      <Box>
                        <Grid gridTemplateColumns='1fr 1fr' paddingTop='20px'>
                          <Box marginRight='10px'>
                            <Card backgroundColor='rgba(240, 255, 244, 1)' borderRadius='0px'>
                              <CardHeader>
                                <Heading size='small' backgroundColor='rgba(198, 246, 213, 1)' border='rgba(104, 211, 145, 1) solid 1px' padding='5px' textAlign='left'>From today's feature article</Heading>
                              </CardHeader>
                              <CardBody>
                                <Box textAlign='justify' fontSize='15px'>
                                  <Image src={Egypttoday} float='left' height='30%' w='30%' margin='10px'/>
                                  <Text as='strong'>Ancient Egyptian literature</Text> was written in the Egyptian language from Ancient Egypt's pharaonic period until the end of Roman domination. Along with Sumerian literature, it is considered the world's earliest literature. Writing in Ancient Egypt (sample pictured) first appeared in the late 4th millennium BC. By the Old Kingdom, literary works included funerary texts, epistles and letters, religious hymns and poems, and commemorative autobiographical texts. Middle Egyptian, the spoken language of the Middle Kingdom, became a classical language preserving a narrative Egyptian literature during the New Kingdom, when Late Egyptian first appeared in writing. Scribes of the New Kingdom canonized and copied many literary texts written in Middle Egyptian, which remained the language used for oral readings of sacred hieroglyphic texts. Ancient Egyptian literature has been preserved on papyrus scrolls and packets, limestone and ceramic ostraca, wooden writing boards, monumental stone edifices, and coffins. (<strong>Full article ...</strong>)
                                  <Text textAlign='left' marginLeft='3rem'>
                                    Recently featured: Angel Reese . Nicholas Hoult . Civil Service Rifles War Memorial<br/>
                                    <Text as='strong' marginLeft='4rem'>Archive . By email . More featured articles . About</Text>
                                  </Text>
                                </Box>
                              </CardBody>
                                <CardHeader>
                                  <Heading size='small' backgroundColor='rgba(198, 246, 213, 1)' border='rgba(104, 211, 145, 1) solid 1px' padding='5px' textAlign='left'>Did You Know?</Heading>
                                </CardHeader>
                                <CardBody>
                                  <Box fontSize='15px'>
                                    <Image src={DidyouKnow} height='30%' w='30%' float='right' margin='10px'/>
                                    <List textAlign='left' >
                                      <UnorderedList>
                                        <ListItem>
                                          .. that the first theatrical run of <Text as='strong'> A Stormy Night </Text> by Ion Luca Caragiale (pictured), featuring a journalist nearly beaten up by the Civic Guard, saw Caragiale nearly beaten up by the Civic Guard?
                                        </ListItem>
                                        <ListItem>
                                          ... that pioneering bodybuilder <Text as='strong'> Hippolyte Triat </Text> was kidnapped by vagabonds at the age of six and sold to a troupe of Italian acrobats?
                                        </ListItem>
                                        <ListItem>
                                          ... that Nigeria seeks the return of the <Text as='strong'> Benin Altar Tusks </Text>, ivory artefacts taken by the British in 1897 and dispersed in Europe?
                                        </ListItem>
                                        <ListItem>
                                          ... that the <Text as='strong'>Cold Cathode Gauge Experiment</Text> could detect gases leaking from an astronaut's life support systems on the Moon's surface?
                                        </ListItem>
                                        <ListItem>
                                          ... that the national women's day of Guinea-Bissau commemorates the death of <Text as='strong'> Titina Silá </Text>, who was killed on this day during the nation's war of independence?
                                        </ListItem>
                                        <ListItem>
                                          ... that the <Text as='strong'>Adly Mansour Transportation Hub</Text> was selected as the best rail project of 2022 by Engineering News-Record?
                                        </ListItem>
                                        <ListItem>
                                          ... that the comic book <Text as='strong'>Nietzsche, se créer liberté</Text> tries to express Friedrich Nietzsche's personality visually?
                                        </ListItem>
                                      </UnorderedList>
                                    </List>
                                    <Text as='strong'>
                                      Archive Start . a new article . Nominate an article
                                    </Text>
                                  </Box>
                                </CardBody>
                              </Card>
                          </Box>
                          <Box>
                            <Card backgroundColor='rgba(235, 248, 255, 1)' borderRadius='0px'>
                              <CardHeader backgroundColor='rgba(235, 248, 255, 1)' borderRadius='0px'>
                                <Heading size='small' backgroundColor='rgba(190, 227, 248, 1)' border='rgba(144, 205, 244, 1) solid 1px' padding='5px' textAlign='left'>In the news</Heading>
                              </CardHeader>
                              <CardBody>
                                <Box fontSize='15px' >
                                  <Image src={MarsFly} height='30%' w='30%' float='right' margin='10px'/>
                                  <List textAlign='left' >
                                    <UnorderedList>
                                      <ListItem>
                                        Following damage to the helicopter's rotors, NASA ends the <Text as='i'><Text as='b'>Ingenuity</Text></Text> (pictured) mission on Mars after almost three years and seventy-two flights.
                                      </ListItem>
                                      <ListItem>
                                        The <Text as='strong'>Ram Mandir</Text>, a temple to Rama, is consecrated at a disputed site in Ayodhya, India.
                                      </ListItem>
                                      <ListItem>
                                        Japan Aerospace Exploration Agency's lunar module <Text as='strong'>SLIM</Text> lands on the Moon.
                                      </ListItem>
                                      <ListItem>
                                        <Text as='strong'>Protests</Text> break out in Bashkortostan, Russia, following the imprisonment of environmental activist Fail Alsynov.
                                      </ListItem>
                                      <ListItem>
                                        Iran launches <Text as='b'>missile strikes</Text> in Pakistan and <Text as='strong'>aerial strikes</Text> in Iraq and Syria, and Pakistan responds with <Text as='strong'>retaliatory airstrikes</Text> .
                                      </ListItem>
                                    </UnorderedList>
                                  </List>
                                  <Text marginTop='10px' textAlign='left'><Text as='b'>Ongoing:</Text> Israel–Hamas war . Myanmar civil war . Red Sea crisis . Russian invasion of Ukraine timeline</Text>
                                  <Text marginTop='10px' textAlign='left'><Text as='b'>Recent Death:</Text> Jack Riddell . Arne Hegerfors . Malcolm Gregson . Sukhbir Singh Gill . Roger Rogerson . David L. Mills</Text>
                                  <Text textAlign='right' as='b'>Nominate an article</Text>
                                </Box>
                              </CardBody>
                                  <CardHeader backgroundColor='rgba(235, 248, 255, 1)' borderRadius='0px'>
                                    <Heading size='small' backgroundColor='rgba(190, 227, 248, 1)' border='rgba(144, 205, 244, 1) solid 1px' padding='5px' textAlign='left'>On this day</Heading>
                                  </CardHeader>
                                  <CardBody>
                                    <Box fontSize='15px' textAlign='left'>
                                      <Text as='strong'>January 31:</Text> Independence Day in <Text as='strong'>Nauru</Text> (1968)
                                      <Image src={onThisDay} float='right' h='20%' w='25%' marginTop='30px'/>
                                      <List textAlign='left'>
                                        <UnorderedList>
                                          <ListItem>
                                            314 – Sylvester I (bust depicted), during whose pontificate many churches in Rome were constructed by Constantine the Great, began his reign as pope.
                                          </ListItem>
                                          <ListItem>
                                            1919 – Intense rioting over labour conditions broke out in Glasgow, Scotland, quelled only when the British government sent tanks to restore order.
                                          </ListItem>
                                          <ListItem>
                                            1997 – Final Fantasy VII, the first video game in the Final Fantasy franchise to use 3-D computer graphics, was released.
                                          </ListItem>
                                          <ListItem>
                                            2007 – Emergency officials in Boston mistakenly identified LED placards depicting characters from Aqua Teen Hunger Force as IEDs, causing a panic.
                                          </ListItem>
                                        </UnorderedList>
                                      </List>
                                      <Text as='b'>
                                        John Francis Regis (b. 1597) . Franz Schubert (b. 1797) . Jackie Robinson (b. 1919) . Adelaide Tambo (d. 2007)
                                      </Text>
                                      <Text>
                                        More anniversaries: January 30 . January 31 . February 1
                                      </Text>
                                      <Text as='strong' marginLeft='10rem'>
                                        Archive . By email . List of days of the year
                                      </Text>
                                    </Box>
                                </CardBody>
                              </Card>
                            </Box>
                        </Grid>
                      </Box>
                      <Box marginTop='10px'>
                        <Card backgroundColor='rgba(250, 245, 255, 1)' borderRadius='0px'>
                          <CardHeader>
                            <Heading size='small' backgroundColor='rgba(233, 216, 253, 1)' border='rgba(214, 188, 250, 1) solid 1px' padding='5px' textAlign='left'>Today's featured picture</Heading>
                          </CardHeader>
                          <CardBody>
                            <Box>
                              <Image src={TodayPicture} float='left' h='40%' w='40%' padding='5px' marginRight='20px'/>	
                              <Box textAlign='justify' margin='0px 20px'>Brookesia thieli, commonly known as Domergue's leaf chameleon, is a species of lizard in the chameleon family, Chamaeleonidae. The species is endemic to eastern Madagascar. It was first described in 1969 by Édouard-Raoul Brygoo and Charles Antoine Domergue. This B. thieli lizard was photographed on a leaf in Andasibe, Madagascar.</Box>
                              <Text fontSize='15px' textAlign='left' marginTop='20px'>Photograph credit: Charles J. Sharp</Text>
                              <Text textAlign='end'>Recently featured: Redcurrant . Cyrano de Bergerac . Space Shuttle Challenger disaster</Text>
                            </Box>
                          </CardBody>
                        </Card>
                        <Card backgroundColor='#F7FAFC' borderRadius='0px' height='100%'>
                          <CardHeader paddingBottom='0px'>
                            <Heading size='small' backgroundColor='rgba(226, 232, 240, 1)' border='#A0AEC0 solid 1px' padding='5px' textAlign='left'>
                              Other areas of Wikipedia
                            </Heading>
                          </CardHeader>
                          <CardBody>
                            <List textAlign='left'>
                              <UnorderedList>
                                <ListItem><Text as='b'>Community portal</Text> – The central hub for editors, with resources, links, tasks, and announcements.</ListItem>
                                <ListItem><Text as='b'>Village pump</Text> – Forum for discussions about Wikipedia itself, including policies and technical issues.</ListItem>
                                <ListItem><Text as='b'>Site news</Text> – Sources of news about Wikipedia and the broader Wikimedia movement.</ListItem>
                                <ListItem><Text as='b'>Teahouse</Text> – Ask basic questions about using or editing Wikipedia.</ListItem>
                                <ListItem><Text as='b'>Help desk</Text> – Ask questions about using or editing Wikipedia.</ListItem>
                                <ListItem><Text as='b'>Reference desk</Text> – Ask research questions about encyclopedic topics.</ListItem>
                                <ListItem><Text as='b'>Content portals</Text> – A unique way to navigate the encyclopedia.</ListItem>
                              </UnorderedList>
                            </List>
                          </CardBody>
                          <CardHeader>
                            <Heading size='small' backgroundColor='rgba(226, 232, 240, 1)' border='#A0AEC0 solid 1px' padding='5px' textAlign='left'>
                              Wikipedia's sister projects
                            </Heading>
                          </CardHeader>
                          <CardBody>
                            <Box textAlign='left'>
                              Wikipedia is written by volunteer editors and hosted by the Wikimedia Foundation, a non-profit organization that also hosts a range of other volunteer projects:
                            </Box>
                            <Grid gridTemplateColumns='1fr 1fr 1fr' padding='10px' gap='5%'>
                              <GridItem>
                                <Flex flexDir='row'>
                                  <Icon as={SiWikimediacommons} w='15%' h='15%' color='#2B6CB0'/>
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Common</Text></Text>
                                    <Text>Free Media Repository</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={MediaWiki} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>MediaWiki</Text></Text>
                                    <Text>Wiki software development</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                              <Flex flexDirection='row'>
                                  <Image src={metaWiki} height='13%' width='13%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Meta-Wiki</Text></Text>
                                    <Text textAlign='left'>Wikimedia project coordination</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiBook} height='13%' width='13%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikibooks</Text></Text>
                                    <Text>Free textbooks and manuals</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiData} height='13%' width='13%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikidata</Text></Text>
                                    <Text>Free knowledge base</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiNews} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikinews</Text></Text>
                                    <Text>Free-content news</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                              <Flex flexDirection='row'>
                                  <Image src={WikiQuote} height='8%' width='8%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikinews</Text></Text>
                                    <Text>Collection of quotations</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiSource} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikisource</Text></Text>
                                    <Text>Free-content library</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiSpecies} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikispecies</Text></Text>
                                    <Text>Directory of species</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={Wikiversity} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikiversity</Text></Text>
                                    <Text>Free learning tools</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={WikiVoyage} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wikivoyage</Text></Text>
                                    <Text>Free travel guide</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                              <GridItem>
                                <Flex flexDirection='row'>
                                  <Image src={Wiktionary} height='15%' width='15%' />
                                  <Box marginLeft='5%'>
                                    <Text textAlign='left' color='blue'><Text as='b'>Wiktionary</Text></Text>
                                    <Text>Dictionary and thesaurus</Text>
                                  </Box>
                                </Flex>
                              </GridItem>
                            </Grid>
                          </CardBody>
                          <CardHeader paddingBottom='0px'>
                            <Heading size='small' backgroundColor='rgba(226, 232, 240, 1)' border='#A0AEC0 solid 1px' padding='5px' textAlign='left'>
                              Wikipedia languages
                            </Heading>
                          </CardHeader>
                          <CardBody>
                            <Box textAlign='left'>
                              This Wikipedia is written in English. Many other Wikipedias are available; some of the largest are listed below.
                            </Box>
                            <Box position='relative' padding='3%'>
                              <Divider border='solid grey 1.5px'/>
                              <AbsoluteCenter px='4' bg='#F7FAFC'> 
                                1,000,000+ articles
                              </AbsoluteCenter>
                            </Box>
                            <Text textAlign='center' textColor='blue'>
                              العربيةمصرى . Deutsch . Español . Français . Italiano . Nederlands . 日本語 . Polski . Português . Русский . Svenska . Українська . Tiếng Việt . 中文
                            </Text>
                            <Box position='relative' padding='3%'>
                              <Divider border='solid grey 1.5px'/>
                              <AbsoluteCenter px='2' bg='#F7FAFC'>
                                250,000+ articles
                              </AbsoluteCenter>
                            </Box>
                            <Text textAlign='center' textColor='blue'>
                              Bahasa Indonesia . Bahasa Melayu . Bân-lâm-gúБългарски . Català . Čeština . Dansk . Esperanto . Euskara . فارسی‎עברית . Հ . այերեն . 한국어 . Magyar . Norsk bokmål . Română . Srpski . Srpskohrvatski . Suomi . Türkçe
                            </Text>
                            <Box position='relative' padding='3%'>
                              <Divider border='solid grey 1.5px'/>
                              <AbsoluteCenter px='2' bg='#F7FAFC'>
                                50,000+ articles
                              </AbsoluteCenter>
                            </Box>
                            <Text textAlign='center' textColor='blue'>
                              Asturianu . বাংলা . Bosanski . کوردی . Eesti . Ελληνικά . Simple English . Frysk . Gaeilge . Galego . Hrvatski . ქართული . Kurdî . Latviešu . Lietuvių . മലയാളം . Македонски . Norsk nynorsk . ਪੰਜਾਬੀ . Shqip . Slovenčina . Slovenščina . ไทย . తెలుగు . اردو . Oʻzbekcha / ўзбекча
                            </Text>
                          </CardBody>
                        </Card>
                        <Box alignContent='right'>
                          <Button background='white' _hover={{background:'#EBF8FF'}} textColor='blue'>
                            48 Language <Icon as={FaAngleDown}/>
                          </Button>
                        </Box>
                      </Box>
                    </TabPanel>
                    <TabPanel>
                      <Box 
                      textAlign='left'
                      fontFamily='arial'
                      >
                        <Text>You do not have permission to edit this page, for the following reasons:</Text>
                        <Box
                        paddingTop='0px'
                        backgroundColor='rgba(247, 250, 252, 1)'
                        >
                          <Flex
                          direction='row' 
                          margin='1.5% 10% 3% 10%'
                          padding='2%'
                          backgroundColor='rgba(237, 242, 247, 0.1)'
                          borderColor='rgba(160, 174, 192, 1)'
                          borderStyle='solid'
                          borderWidth='1.5px 1.5px 1.5px 8px'
                          >
                            <Icon as={BiSolidLock} height='5%' width='5%'/>
                            <Text as='b' fontSize='18px' textAlign='center' marginLeft='12%'> This page is <Link color='blue'>transcluded</Link> in multiple <Link color='blue'>cascade-protected</Link> pages, so only <br/> <Link color='blue'>administrators</Link> can edit it.</Text>
                          </Flex>
                          <Grid gridTemplateColumns='1fr 1fr' margin='2%'>
                            <GridItem>
                              <Text as='b' fontSize='18px' fontFamily='Georgia'>Why is this Page Protected?</Text>
                              <List fontSize='14px'>
                                <UnorderedList>
                                  <ListItem>
                                    Cascading protection is used to prevent vandalism to particularly visible pages, such as the <b>Main Page</b> and a few <Link textColor='blue'>very highly used templates.</Link> 
                                  </ListItem>
                                  <ListItem>
                                    This page is <Text as='span' textColor='blue'>transcluded</Text> in the following pages, which are protected with the "cascading" option:
                                    <List>
                                      <UnorderedList>
                                        <ListItem>Wikipedia:Cascade-protected items/Main Page</ListItem>
                                        <ListItem>Wikipedia:Cascade-protected items/Main Page/2</ListItem>
                                        <ListItem>Wikipedia:Cascade-protected items/Main Page/3</ListItem>
                                        <ListItem>Wikipedia:Cascade-protected items/Main Page/4</ListItem>
                                        <ListItem>Wikipedia:Cascade-protected items/Main Page/5</ListItem>
                                      </UnorderedList>
                                    </List>
                                  </ListItem>
                                </UnorderedList>
                              </List>
                            </GridItem>
                            <GridItem>
                              <Text as='b' fontSize='18px' fontFamily='Georgia'>What can I do?</Text>
                              <List fontSize='14px'>
                                <UnorderedList>
                                  <ListItem>Register for an account if you don't already have one</ListItem>
                                  <ListItem>Visit the Sandbox to make test edits</ListItem>
                                  <ListItem>Check out the Tutorial to learn more about editing</ListItem>
                                  <ListItem>Report errors on the Main Page</ListItem>
                                  <ListItem>If you wrote any text, please save it temporarily to your device until you can edit this page.</ListItem>
                                </UnorderedList>
                              </List>
                            </GridItem>
                          </Grid>
                        </Box>
                        <Divider border='solid grey 0.5px'/>
                        <Text>You can view and copy the source of this page:</Text>
                        <Box>
                          <Code overflowX='hidden' overflowY='auto' textAlign='justify' height='300px'>
                            <Stack direction={['column', 'row']}/>
                            {sourceCode.map((e)=>{
                              return <Box>{e}</Box>
                            })}
                          </Code>
                        </Box>
                        <Text>Pages transcluded onto the current version of this page <Text as='i' color='blue'>(help)</Text>:</Text>
                        <List marginTop='20px'>
                          <UnorderedList>
                            <ListItem>Wikipedia:Main Page/Tomorrow (view source) (protected)</ListItem>
                            <ListItem>Wikipedia:Main Page/styles.css (view source) (protected)</ListItem>
                            <ListItem>Wikipedia:Selected anniversaries/February 14 (edit)</ListItem>
                            <ListItem>Wikipedia:Selected anniversaries/February 15 (edit)</ListItem>
                            <ListItem>Wikipedia:Today's featured article/February 14, 2024 (edit)</ListItem>
                            <ListItem>Wikipedia:Today's featured article/February 15, 2024 (edit)</ListItem>
                            <ListItem>Template:Born and died list (view source) (protected)</ListItem>
                            <ListItem>Template:Convert (view source) (template editor protected)</ListItem>
                            <ListItem>Template:DYK bottom prep notice (view source) (semi-protected)</ListItem>
                            <ListItem>Template:DYKbotdo (view source) (protected)</ListItem>
                            <ListItem>Template:Did you know (view source) (protected)</ListItem>
                            <ListItem>Template:Did you know/Queue/4 (view source) (protected)</ListItem>
                            <ListItem>Template:Did you know/Queue/Next (view source) (protected)</ListItem>
                            <ListItem>Template:Endflatlist (view source) (template editor protected)</ListItem>
                            <ListItem>Template:Endplainlist (view source) (template editor protected)</ListItem>
                          </UnorderedList>
                        </List>
                        <Box marginTop='20px' float='right' marginRight='20px'>
                          <Popover>
                            <PopoverTrigger>
                              <Button backgroundColor='white' _hover={{backgroundColor:'#EBF8FF'}}>Add Language</Button>
                            </PopoverTrigger>
                            <PopoverContent>
                              <PopoverArrow/>
                              <PopoverCloseButton/>
                              <PopoverHeader>
                                <InputGroup>
                                  <InputLeftAddon backgroundColor='white' border='none'>
                                   <BiSearch/>
                                  </InputLeftAddon>
                                  <Input border='none' />
                                </InputGroup>
                              </PopoverHeader>
                              <PopoverBody>
                                <Stack padding='12%' fontSize='14px'>
                                  <Text as='b' fontSize='16px'>No Language yet</Text>
                                  <Text>No language are available for now</Text>
                                  <Text textColor='blue'><Icon as={DiAptana}/>Open Language Setting</Text>
                                </Stack>
                              </PopoverBody>
                            </PopoverContent>
                          </Popover>
                        </Box>
                      </Box>
                    </TabPanel>
                    <TabPanel>
                      <Box textAlign='left'>
                        <Text textColor='blue' textAlign='left' fontSize='15px' >View logs for this page (view filter log)</Text>
                        <Box border='solid grey 0.5px' width='100%'>
                          <Accordion defaultIndex={[0]} allowMultiple allowToggle>
                            <AccordionItem>
                              <h2>
                                <AccordionButton _hover={{backgroundColor:'white'}}>
                                  <Box as='b' flex='1' textAlign='left'>
                                    <AccordionIcon/>
                                    Filter Revision
                                  </Box>
                                </AccordionButton>
                              </h2>
                              <AccordionPanel paddingBottom='4'>
                                <Text>To Date:</Text>
                                <Input placeholder='Hello' type='date' width='30%' borderRadius='0px'/>
                                <Text>Tag filter:</Text>
                                <Select placeholder=' '>
                                  <option value='2017 wikitext editor'>2017 wikitext editor</option>
                                  <option value='Advanced mobile edit'>Advanced mobile edit</option>
                                  <option value='Android app edit'>Android app edit</option>
                                  <option value='AntiVandal'>AntiVandal</option>
                                  <option value='Article for deletion template removed'>Article for deletion template removed</option>
                                  <option value='Automatic insertion of extraneous formatting'>Automatic insertion of extraneous formatting</option>
                                  <option value='AWB'>AWB</option>
                                  <option value='Barnsworth[1.0]'>Barnsworth[1.0]</option>
                                  <option value='blanking'>blanking</option>
                                  <option value='Blanking'>Blanking</option>
                                  <option value='Bot in trial'>Bot in trial</option>
                                  <option value='CD'>CD</option>
                                  <option value='Deputy'>Deputy</option>
                                  <option value='Huggle'>Huggle</option>
                                </Select>
                                <Button backgroundColor='rgba(43, 108, 176, 1)' borderRadius='0px' textColor='white' marginTop='20px'>
                                  Show revisions
                                </Button>
                              </AccordionPanel>
                            </AccordionItem>
                          </Accordion>
                        </Box>
                        <Text as='span'>External tools: 
                        <Link color='blue' isExternal>Find addition/removal<ExternalLinkIcon marginX='3px' height='15px'/> </Link> • 
                        <Link color='blue' isExternal> Find edits by user <ExternalLinkIcon marginX='3px' height='15px'/></Link> • 
                        <Link color='blue' isExternal>Page statistics <ExternalLinkIcon marginX='3px' height='15px'/></Link> • 
                        <Link textColor='blue' isExternal>Pageviews <ExternalLinkIcon marginX='3px' height='15px'/></Link> • 
                        <Link textColor='blue' isExternal>Fix dead links<ExternalLinkIcon marginX='3px' height='15px'/></Link>
                        </Text>
                        <Divider border='solid #A0AEC0 1px'/>
                        <Text>For any version listed below, click on its date to view it. For more help, see <Link color='blue'>Help:Page history</Link> and <Link color='blue'>Help:Edit summary.</Link> (cur) = difference from current version, (prev) = difference from preceding version, <b>m</b> = <Link textColor='blue'>minor edit</Link>, → = <Link textColor='blue'>section edit</Link>, ← = <Link textColor='blue'>automatic edit summary</Link></Text>
                        <Text marginTop='30px'>
                          (newest | oldest) View (newer 50 | older 50) (20 | 50 | 100 | 250 | 500)
                        </Text>
                        <Button 
                        backgroundColor='#EDF2F7' 
                        marginTop='5px'
                        borderRadius='0px'
                        border='solid grey 1px'
                        >
                          Compared Selected revisions
                        </Button>
                        <List>
                          <UnorderedList>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                              <Divider border='dotted grey 1px'/>
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                            <ListItem>
                              (cur | prev) <Radio type='radio'>23:59, 12 December 2023‎</Radio>  Firefangledfeathers talk contribs‎  2,987 bytes −1‎  back to singular FA Tag: Manual revert
                            </ListItem>
                          </UnorderedList>
                        </List>
                      </Box>
                    </TabPanel>
                  </TabPanels>  
                </Tabs>
                :
                <Tabs align='end' onChange={(e)=>setTab1(e)}>
                  <TabList>
                    <Tab>Read</Tab>
                    <Tab>View Source</Tab>
                    <Tab>View history</Tab>
                    <Box>
                      <Popover>
                        <PopoverTrigger>
                          <Button bg='none' fontWeight='none' _hover={{bg:'#F7FAFC'}}>
                            Tools
                            <FaAngleDown/>
                          </Button>
                        </PopoverTrigger>
                        <Portal>
                          <PopoverContent>
                            <PopoverArrow />
                            <PopoverHeader>
                              Tools
                            </PopoverHeader>
                            <PopoverHeader>
                              General
                            </PopoverHeader>
                            <PopoverBody>
                              <Stack direction='column'>
                                <Text textColor='blue'>What links here</Text>
                                <Text>Related changes</Text>
                                <Text>Special pages</Text>
                                <Text>Permanant link</Text>
                                <Text>Page information</Text>
                              </Stack>
                            </PopoverBody>
                          </PopoverContent>
                        </Portal>
                      </Popover>
                    </Box>
                  </TabList>
                  <TabPanels>
                    <TabPanel>
                      Hello
                    </TabPanel>
                    <TabPanel> World </TabPanel>
                    <TabPanel> Nice </TabPanel>
                  </TabPanels>
                </Tabs>
              }
            </Tabs>
          </Box>
        </Grid>
        <Box padding='5% 10% 5%'>
          <Divider border='solid grey 1px'/>
          <Text fontSize='12px' marginTop='20px'>
            Text is available under the Creative Commons Attribution-ShareAlike License 4.0; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.
          </Text>
          <Text fontSize='12px' marginTop='20px'>
            Privacy policy . About Wikipedia . Disclaimers . Contact Wikipedia . Code of Conduct . Developers . Statistics . Cookie statement . Mobile view
          </Text>
        </Box>
      </>
    )
  }
  
  export default FrontPage
  
  