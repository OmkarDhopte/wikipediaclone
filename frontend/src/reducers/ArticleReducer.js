import {
    ARTICLE_REQUEST,
    ARTICLE_SUCESS,
    ARTICLE_FAIL
} from '../constants/ArticleConstants'

export const ArticleReducer = (state = { article: [] }, action) =>{
    switch (action.type) {
        case ARTICLE_REQUEST:
            return {loading: true, article:[]}
        case ARTICLE_SUCESS:
            return {loading:false, payload:action.payload}
        case ARTICLE_FAIL:
            return {loading:false, error:action.payload}
        default:
            return state
    }
}
